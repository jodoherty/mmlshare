TOTAL_SONGS	equ	$01
SOUND_GENERATOR	equ	$01
PTRDPCM		equ	 4
PTRFDS		equ	 5
PTRVRC7		equ	 5
PTRVRC6		equ	 5
PTRN106		equ	 8
PTRFME7		equ	 8
PTRMMC5		equ	 8
PTR_TRACK_END		equ	 8
PITCH_CORRECTION		equ	 0
DPCM_RESTSTOP		equ	 0
DPCM_BANKSWITCH		equ	 0
DPCM_EXTRA_BANK_START		equ	 2
BANK_MAX_IN_4KB		equ	(1 + 0)*2+1
ALLOW_BANK_SWITCH		equ	0
TITLE	.macro
	db	$50, $68, $61, $6e, $74, $6f, $6d, $20
	db	$6f, $66, $20, $74, $68, $65, $20, $44
	db	$69, $6e, $6f, $73, $61, $75, $72, $73
	db	$00, $00, $00, $00, $00, $00, $00, $00
	.endm
COMPOSER	.macro
	db	$4a, $61, $6d, $65, $73, $20, $4f, $27
	db	$44, $6f, $68, $65, $72, $74, $79, $00
	db	$00, $00, $00, $00, $00, $00, $00, $00
	db	$00, $00, $00, $00, $00, $00, $00, $00
	.endm
MAKER	.macro
	db	$54, $68, $65, $20, $4d, $61, $6b, $65
	db	$72, $00, $00, $00, $00, $00, $00, $00
	db	$00, $00, $00, $00, $00, $00, $00, $00
	db	$00, $00, $00, $00, $00, $00, $00, $00
	.endm



import data
import tools
from mmlsharedb_sqlite import DB

db = DB()
c = db.get_cursor()

schemafile = open('data.sql')
schema = schemafile.read()
schemafile.close()

c.executescript(schema)


users = data.db.users.find()
for user in users:
  c.execute("""INSERT into USERS (username, password, email, profile, confirmed, regdate)
                  VALUES(?,?,?,?,?,?)""", 
            [user['username'],
             user['password'],
             user['email'],
             user['profile'],
             1,
             user['regdate'].isoformat()])
  
tracks = data.db.tracks.find()
for track in tracks:
  c.execute("""INSERT into tracks (bid, title, artist, date, last_modified, body, finished)
                VALUES(?,?,?,?,?,?,?)""",
            [str(track['_id']),
             track['title'],
             track['artist'],
             track['date'].isoformat(),
             track['date'].isoformat(),
             tools.generate_mml(track),
             0])
  tid = c.lastrowid
  try:
    oldfile = open('./static/nsf/' + str(track['_id']) + '.nsf')
    newfile = open('./static/nsf/' + str(tid) + '.nsf', 'w')
    newfile.write(oldfile.read())
    oldfile.close()
    newfile.close()
  except:
    pass
  if track.has_key('comments'):
    for comment in track['comments']:
      c.execute("""INSERT into comments (track_id, author, message, date) VALUES(?,?,?,?)""",
                [tid, comment['author'], comment['message'], comment['date'].isoformat()])

samples = data.db.samples.find()
for sample in samples:
  if not sample.has_key('name'):
    continue
  if not sample.has_key('game'):
    continue
  if not sample.has_key('instrument'):
    continue
  c.execute("INSERT into samples (name, instrument, game) VALUES (?,?,?)",
            [sample['name'], sample['instrument'], sample['game']])

c.close()
db.close()




create table users (
  id integer primary key,
  username text,
  password text,
  email text,
  profile text,
  confirmed integer,
  regdate text
);

create table comments (
  id integer primary key,
  track_id integer,
  author text,
  message text,
  date text
);

create table samples (
  id integer primary key,
  name text,
  instrument text,
  game text
);

create table tracks (
  id integer primary key,
  bid text,
  title text,
  artist text,
  date text,
  last_modified text,
  last_finished text,
  body text,
  finished boolean
);

import sqlite3
import datetime,time
import sha

def hash_password(password):
  return sha.sha(password).hexdigest()
def convert_date(date_string):
  if not date_string:
    return None
  return datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%f")
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
class DB:
  def __init__(self):
    self.opened = False
  def open(self):
    if not self.opened:
      self.conn = sqlite3.connect('data/metainfo.db',
          detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
      self.conn.row_factory = dict_factory
      self.opened = True
  def close(self):
    if self.opened:
      self.conn.commit()
      self.conn.close()
      self.opened = False
  def get_cursor(self):
    if not self.opened:
      self.open()
    return self.conn.cursor()
  def hash_password(self, password):
    return hash_password(password)
  # Users
  def make_user(self, row):
    if row:
      return {'user_id': int(row['id']),
              '_id': row['id'],
              'username': row['username'],
              'password': row['password'],
              'email': row['email'],
              'profile': row['profile'],
              'confirmed': row['confirmed'] == 1,
              'regdate': convert_date(row['regdate']) }
    return None
  def username_exists(self, username):
    return self.get_user_ic(username) != None
  def get_user_by_id(self, user_id):
    if user_id == None:
      return None
    try:
      self.open()
      rows = self.conn.execute("select * from users where id = ?", [int(user_id)])
      for row in rows:
        return self.make_user(row)
      return None
    finally:
      self.close()
  def get_user_ic(self, username):
    try:
      self.open()
      rows = self.conn.execute("select * from users where username = ?", [username])
      for row in rows:
        return self.make_user(row)
      return None
    finally:
      self.close()
  def add_user(self, user):
    try:
      self.open()
      values     = [user['username'],
                    user['password'],
                    user['email'],
                    user['profile'],
                    user['confirmed'],
                    user['regdate']]
      c = self.conn.cursor()
      res = c.execute("""insert into users (username, password, email, 
                            profile, confirmed, regdate)
                          values (?, ?, ?, ?, ?, ?)""", values)
      c.close()
      return res
    finally:
      self.close()
  def verify_user(self, username, password):
    user = self.get_user_ic(username)
    if not user:
      return False
    return user['password'] == hash_password(password)
  # Samples
  def make_sample(self, row):
    if row:
      return {'id': int(row['id']),
              '_id': row['id'],
              'name': row['name'],
              'game': row['game'],
              'instrument': row['instrument'] }
    return None
  def add_sample(self, **kargs):
    try:
      self.open()
      values = [kargs['name'], kargs['game'], kargs['instrument']]
      return self.conn.execute("""insert into samples (name, game, instrument)
                                  values(?,?,?)""", values)
    finally:
      self.close()
  def get_sample(self, name):
    try:
      self.open()
      rows = self.conn.execute("select * from samples where name = ?", [name])
      for row in rows:
        return self.make_sample(row)
    finally:
      self.close()
  def get_sample_games(self):
    try:
      self.open()
      rows = self.conn.execute("select distinct game from samples")
      games = [row['game'] for row in rows]
      return games
    finally:
      self.close()
  def get_sample_instruments(self):
    try:
      self.open()
      rows = self.conn.execute("select distinct instrument from samples")
      instruments = [row['instrument'] for row in rows]
      return instruments
    finally:
      self.close()
  def get_samples(self, games=[], instruments=[]):
    try:
      self.open()
      samples = []
      for game in games:
        rows = self.conn.execute("select * from samples where game = ?", [game])
        samples += [self.make_sample(row) for row in rows]
      for instrument in instruments:
        rows = self.conn.execute("select * from samples where instrument = ?", [instrument])
        samples += [self.make_sample(row) for row in rows]
      if (not games) and (not instruments):
        rows = self.conn.execute("select * from samples")
        samples = [self.make_sample(row) for row in rows]
      return samples
    finally:
      self.close()
  # Tracks
  def count_tracks(self, artist=None, finished=False):
    try:
      self.open()
      if artist:
        rows = self.conn.execute("""SELECT count(*) as count from tracks
                                    where artist = ?""", [artist])
      else: 
        rows = self.conn.execute("""SELECT count(*) as count from tracks
                                    where finished = ?""", [int(finished)])
      for row in rows:
        return int(row['count'])
    finally:
      self.close()
  def get_tracks(self, offset=0, limit=20, order='date', artist=None, finished=False):
    try:
      self.open()
      rows = []
      if artist:
        rows = self.conn.execute("""SELECT * from tracks where 
                                    artist = ?
                                    ORDER BY last_modified DESC LIMIT ?,?""", 
                                   [artist, offset, limit])
      elif finished:
        rows = self.conn.execute("""SELECT * from tracks where finished = ?
                                    ORDER BY last_finished DESC LIMIT ?,?""", 
                                   [int(finished), offset, limit])
      else:
        rows = self.conn.execute("""SELECT * from tracks where finished = ?
                                    ORDER BY last_modified DESC LIMIT ?,?""", 
                                   [int(finished), offset, limit])
      tracks = [dict(row) for row in rows]
      for track in tracks:
        track['date'] = convert_date(track['date'])
        track['last_modified'] = convert_date(track['last_modified'])
        track['last_finished'] = convert_date(track['last_finished'])
      return tracks
    finally:
      self.close()
  def get_track(self, track_id):
    try:
      self.open()
      if type(track_id) == type(1) or len(track_id) < 10:
        track_id = int(track_id)
        rows = self.conn.execute("""SELECT * from tracks where id = ?""",
                                 [track_id])
      else:
        rows = self.conn.execute("""SELECT * from tracks where bid = ?""",
                                 [track_id])
      track = {}
      for row in rows:
        track = dict(row)
      return track
    finally:
      self.close()
  def remove_track(self, track_id):
    try:
      self.open()
      self.conn.execute("DELETE from tracks where id = ?",
                        [track_id])
      self.conn.execute("DELETE from comments where track_id = ?",
                        [track_id])
    finally:
      self.close()
  def create_track(self, track):
    try:
      self.open()
      c = self.conn.cursor()
      c.execute("""INSERT into tracks (title, artist, body, date, last_modified, finished)
                   values (?,?,?,?,?,0)""",
                        [track['title'], track['artist'], track['body'],
                         datetime.datetime.now().isoformat(),
                         datetime.datetime.now().isoformat()])
      tid = c.lastrowid
      return tid
    finally:
      c.close()
      self.close()
  def update_track(self, track_id, track):
    try:
      self.open()
      self.conn.execute("""UPDATE tracks 
                           set title = ?,
                               artist = ?,
                               date = ?,
                               last_modified = ?,
                               last_finished = ?,
                               body = ?,
                               finished = ?
                           where id = ?""",
                        [track['title'],
                         track['artist'],
                         track['date'],
                         track['last_modified'],
                         track['last_finished'],
                         track['body'],
                         track['finished'],
                         int(track_id)])
    finally:
      self.close()
  # Comments
  def get_comments(self, track_id):
    try:
      self.open()
      rows = self.conn.execute("SELECT * from comments where track_id = ?", [track_id])
      comments = [dict(row) for row in rows]
      for comment in comments:
        comment['date'] = convert_date(comment['date'])
      return comments
    finally:
      self.close()
  def add_comment(self, track_id, comment):
    try:
      self.open()
      self.conn.execute("""INSERT into comments (track_id, author, 
                                    message, date) VALUES(?,?,?,?)""",
                        [track_id, comment['author'], comment['message'], comment['date']])
    finally:
      self.close()
  def get_recent_comments(self, user):
    try:
      self.open()
      rows = self.conn.execute("""SELECT tracks.id as track_id, 
                                         tracks.title as track_title,
                                         comments.author as author, 
                                         comments.message as message,
                                         comments.date as date
                                  FROM tracks, comments
                                  WHERE tracks.artist = ?
                                    AND tracks.id = comments.track_id
                                  ORDER BY comments.date DESC
                                  LIMIT 0,30""",
                               [user])
      return [dict(row) for row in rows]
    finally:
      self.close()


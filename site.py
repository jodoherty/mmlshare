#!/usr/bin/env python2.6

import tools
import config
import datetime
from mmlsharedb_sqlite import DB

# Utility functions
from mako.lookup import TemplateLookup
template_lookup = TemplateLookup(directories=['templates'], 
                                 module_directory = 'tcache')

data = DB()

def get_template(pagename):
  from mako.template import Template
  return template_lookup.get_template(pagename + '.html')
  
def render(name, **context):
  template = get_template(name)
  user = data.get_user_by_id(cherrypy.session.get('user_id'))
  return template.render_unicode(rights = rights(),
                                 user = user,
                                 **context).encode('utf-8')
  
def error(errormsg):
  return render('error', error = errormsg)

def current_user():
  return data.get_user_by_id(cherrypy.session.get('user_id'))

# Access control

def rights():
  user = current_user()
  if not user:
    return []
  if not 'rights' in list(user.keys()):
    return ['sample', 'track', 'forum', 'comment']
  # In the future we'll store rights in the database
  return user['rights']
  
class requires_auth:
  """ Enforce user rights and priviliges with this decorator. """
  def __init__(self, right):
    self.right = right
  
  def __call__(self, f):
    def call_if_has_right(*args, **keys):
      if not current_user():
        return error('This action requires you to be logged in.')
      if not self.right in rights():
        return error('You don\'t have the necessary %s rights' % right)
      return f(*args, **keys)
    return call_if_has_right

# CherryPy handlers/view objects
import cherrypy
import json
import re, cgi


# Samples
class MMLSamples:
  @cherrypy.expose
  def index(self, offset=0, limit=20, tags=[]):
    return ''
  
  @cherrypy.expose
  def games(self):
    return json.dumps(data.get_sample_games())
    
  @cherrypy.expose
  def instruments(self):
    return json.dumps(data.get_sample_instruments())
    
  @cherrypy.expose
  def find(self, games=[], instruments=[]):
    if not isinstance(games, type([])):
      games = [games]
    if not isinstance(instruments, type([])):
      instruments = [instruments]
    return json.dumps([sample for sample in
                       data.get_samples(games = games,
                                        instruments = instruments)])
  
  @cherrypy.expose
  @requires_auth('sample')
  def mine(self, offset=0, limit=20):
    """ Return an indexed listing of all samples uploaded by the user. """
    import glob, os.path
    username = current_user()['username']
    offset = int(offset)
    limit = int(limit)
    sampledir = config.basedir + '/samples/' + current_user()['username']
    if not os.path.exists(sampledir):
      samples = []
      count = 0
    else:
      samplenames = [os.path.splitext(os.path.basename(p))[0]
                     for p in glob.glob(sampledir + '/*')]
      count = len(samplenames)
      samples = filter(None,
                       [data.get_sample(current_user()['username'] + '/' + s)
                        for s in samplenames[offset:limit]])
    return render('samples_mine', samples=samples, offset=offset,
                  limit=limit, count=count)

  @cherrypy.expose
  @requires_auth('sample')
  def upload(self, samplefile, name='', instrument='', game=''):
    """ Upload a new sample """
    import os.path
    if not name:
      return error('Sample must have a name.')
    if not file:
      return error('Sample file not provided.')
    if not tools.is_path_secure(name):
      return error('Sample name contained invalid characters.')
    name = current_user()['username'] + '/' + name;
    filename = config.basedir + '/samples/' + name + '.dmc';
    sampledir = config.basedir + '/samples/' + current_user()['username']
    if not os.path.exists(sampledir):
      os.mkdir(sampledir)
    targetfile = open(filename, 'w')
    while True:
      if not targetfile.write(samplefile.file.read(8192)):
        break
    targetfile.close()
    sample = {'name': name,
              'instrument': instrument,
              'game': game}
    data.add_sample(**sample)
    raise cherrypy.HTTPRedirect('/samples/mine/')


# Tracks
class MMLTracks:
  """Handles the browsing, viewing, and editing of MML tracks."""

  @cherrypy.expose
  def index(self, offset=0, limit=20):
    """ Return an indexed listing of wip tracks. """
    offset = int(offset)
    limit = int(limit)
    tracks = data.get_tracks(offset, limit, finished=False)
    count = data.count_tracks()
    return render('tracks_index', tracks=tracks, offset=offset,
                  limit=limit, count=count, finished=False)

  @cherrypy.expose
  def finished(self, offset=0, limit=20):
    """ Return an indexed listing of finished tracks. """
    offset = int(offset)
    limit = int(limit)
    tracks = data.get_tracks(offset, limit, finished=True)
    count = data.count_tracks(finished=True)
    return render('tracks_index', tracks=tracks, offset=offset,
                  limit=limit, count=count, finished=True)
    
  @cherrypy.expose
  @requires_auth('track')
  def mine(self, offset=0, limit=20):
    """ Return an indexed listing of all tracks by the user. """
    username = current_user()['username']
    offset = int(offset)
    limit = int(limit)
    tracks = data.get_tracks(offset, limit, artist=username)
    count = data.count_tracks(artist=username)
    return render('tracks_mine', tracks=tracks, offset=offset,
                  limit=limit, count=count)

  @cherrypy.expose
  def view(self, track_id):
    """ View and play the specified track. """
    if len(track_id) < 10:
      track_id = int(track_id)
    track = data.get_track(track_id)
    if not track:
      return error('Track not found.')
    track['comments'] = data.get_comments(track['id'])
    return render('tracks_view', track=track,
                  mml=track['body'])

  @cherrypy.expose
  @requires_auth('track')
  def add(self, title=None):
    if (cherrypy.request.method == 'POST'):
      if not title:
        return error('Track contained no title.')
      if not tools.is_path_secure(title):
        return error('Title contained illegal characters.')
      track = {'title': title.decode('utf-8'),
               'artist': current_user()['username'],
               'body': """#TITLE %s
#COMPOSER %s

;===========================
;Samples :

;===========================
;Macros :

;===========================
;Song :

A v14 @q50 @2 L n67 n55 n55 n55 ;Metronome""" % (title, current_user()['username'])}
      new_id = data.create_track(track)
      raise cherrypy.HTTPRedirect('/tracks/edit/' + str(new_id))
    return render('tracks_add')

  @cherrypy.expose
  @requires_auth('track')
  def publish(self, tracks = []):
    if cherrypy.request.method == 'POST':
      def pub(track_id):
        track = data.get_track(track_id)
        if track and track['artist'] == current_user()['username']:
          track['finished'] = True
          track['last_finished'] = datetime.datetime.now().isoformat()
          data.update_track(track_id, track)
      if type(tracks) == type([]):
        for track_id in tracks:
          pub(track_id)
      else:
        pub(tracks)
    raise cherrypy.HTTPRedirect('/tracks/mine')
        
    
  @cherrypy.expose
  @requires_auth('track')
  def remove(self, tracks = []):
    if cherrypy.request.method == 'POST':
      titles = []
      if not (type(tracks) == type([])):
        tracks = [tracks]
      for track_id in tracks:
        track = data.get_track(track_id)
        if not track:
          continue
        if not (track['artist'] == current_user()['username']):
          continue
        titles.append(track['title'])
        data.remove_track(track['id'])
      if not titles:
        return error('No tracks removed.')
      return json.dumps({'titles': titles})
    return error('No tracks submitted for removal.')

  @cherrypy.expose
  @requires_auth('track')
  def edit(self, track_id = ''):
    """ Edit a track, or if now track_id is given, add a new track. """
    track_action = "Edit Track"
    track = data.get_track(int(track_id))
    if not track:
      raise cherrypy.HTTPRedirect('/tracks/add')
    else:
      if not (track['artist']
              == current_user()['username']):
        return error('You don\'t have permission to edit this track.')
    return render('tracks_edit', track_id = track_id, track = track,
                  track_action = track_action)

  @cherrypy.expose
  @requires_auth('track')
  def doEdit(self, track_id = -1, title = None, body = None):
    """ AJAX request handler for editing tracks."""
    if not title:
      return json.dumps({'result': 'error', 'error': 'notitle'})
    elif not body:
      return json.dumps({'result': 'error', 'error': 'nobody'})
    if track_id == -1:
      return json.dumps({'result': 'error', 'error': 'noexist'})
    track = data.get_track(int(track_id))
    if not track:
      return json.dumps({'result': 'error', 'error': 'noexist'})
    track['title'] = title.decode('utf-8')
    track['artist'] = current_user()['username']
    track['body'] = body.decode('utf-8')
    track['last_modified'] = datetime.datetime.now().isoformat()
    data.update_track(track_id, track)
    result_type = 'update'
    build_results = tools.build_nsf(track_id, track)
    return json.dumps({'result': 'success', 
                       'type': result_type,
                       'build_results': build_results, 
                       'track_id': track_id})
                       
  @cherrypy.expose
  @requires_auth('comment')
  def comment(self, track_id=None, message=''):
    if not track_id or not data.get_track(int(track_id)):
      return error('Track doesn\'t exist.')
    if not message.strip():
      return error('Attempted posting blank comment.')
    comment = {'author': current_user()['username'],
               'message': message.decode('utf-8'),
               'date': datetime.datetime.now().isoformat()}
    data.add_comment(track_id, comment)
    raise cherrypy.HTTPRedirect('/tracks/view/' + track_id)

  @cherrypy.expose
  def nsf(self, track_id=None):
    import os.path
    if not track_id or not data.get_track(int(track_id)):
      return error('Track doesn\'t exist.')
    if not os.path.exists(config.basedir + '/static/nsf/' + track_id + '.nsf'):
      return error('Track hasn\'t been built.')
    cherrypy.response.headers['Expires'] = 'Fri, 30 Oct 1998 14:19:41 GMT'
    cherrypy.response.headers['Pragma'] = 'nocache'
    cherrypy.response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    cherrypy.response.headers['Content-Type'] = 'application/octet-stream'
    try:
      nsffile = open(config.basedir + '/static/nsf/' + track_id + '.nsf', 'r')
      return nsffile.read()
    finally:
      nsffile.close()

# Main Site
class MMLShareRoot:
  """ Home page and user authentication management class """

  # Set up track subtree
  tracks = MMLTracks()
  samples = MMLSamples()

  @cherrypy.expose
  def index(self):
    """ Provide news and updates, recent listings, user dashboard, etc."""
    return render('index')

  @cherrypy.expose
  def comments(self):
    user = current_user()
    if user:
      comments = data.get_recent_comments(user['username'])
      return render('comments', comments=comments)
    else:
      raise cherrypy.HTTPRedirect('/')

  @cherrypy.expose
  def login(self):
    """ Provide a login prompt. """
    user = current_user()
    if user:
      return error("Already logged in as " + user['username'])
    return render('login')

  @cherrypy.expose
  def logout(self):
    """ Provide the user with a means to log out. """
    cherrypy.lib.sessions.expire()
    raise cherrypy.HTTPRedirect('/')

  @cherrypy.expose
  def doLogin(self, username='', password=''):
    """Handle a login request. """
    if not data.verify_user(username, password):
      return error('Login failed: Invalid username and password.')
    user = data.get_user_ic(username)
    cherrypy.session['user_id'] = user['_id']
    raise cherrypy.HTTPRedirect('/')

  @cherrypy.expose
  def register(self, username = '', password = '', email = ''):
    user = current_user()
    if user:
      return error("Already logged in as " + user['username'])
    username = cgi.escape(username.decode('utf-8').strip())
    email = cgi.escape(email.decode('utf-8').strip())
    user = {'username': username,
            'password': data.hash_password(password),
            'email': email,
            'profile': 'New Member',
            'confirmed': 0,
            'regdate': datetime.datetime.now().isoformat()}
    if cherrypy.request.method == 'POST':
      def register_error(error):
        return render('register', 
                      newuser = user, 
                      error = error)
      if not username:
        return register_error('No username provided.')
      if not tools.is_path_secure(username):
        return register_error('Invalid username provided.')
      if data.username_exists(username):
        return register_error('Username already exists.')
      if not password:
        return register_error('No password provided.')
      if not email:
        return register_error('No email address provided.')
      data.add_user(user)
      return self.doLogin(username=username, password=password)
    return render('register', newuser = user, error = "")

# Setup and start the server
cherrypy.quickstart(MMLShareRoot(), '/', 'config.ini')




function save() {
  var result = '';
  $.ajax({
    url: '/tracks/doEdit',
    type: 'POST',
    data: $('#track').serialize(),
    dataType: 'json',
    success: function(result, textStatus) {
      if (result['result'] == 'error') {
        var errormsg = '';
        if (result['error'] == 'notitle') {
          errormsg = 'Title is blank.';
        } else if (result['error'] == 'nobody') {
          errormsg = 'Blank MML file.';
        }
        message('Error:', errormsg);
        return false;
      } else {
        if (result['type'] == 'add') 
          document.location = '/tracks/edit/' + result['track_id'];
        else if (result['build_results'] != 'success')
          message('Failed to build NSF file:', result['build_results']);
        else
          disable_savebutton();
      }
    }
  });
}

function enable_player() {
  
}

function disable_player() {
  
}

function enable_savebutton() {
  $('#savebutton')[0].disabled = false;
  $('#editbody').unbind('keydown');
  $('input:text[name=title]').unbind('keydown');
  shortcut.add("Ctrl+S",function(e) {
    save();
    if (e.preventDefault) e.preventDefault();
    else e.returnValue = false;
    return false;
  });
  disable_player();
}

function disable_savebutton() {
  $('#savebutton')[0].disabled = true;
  $('#editbody').keydown(enable_savebutton);
  $('input:text[name=title]').keydown(enable_savebutton);
  shortcut.remove("Ctrl+S");
  enable_player();
}

function clearmsg() {
  $('#message').css('display', 'none');
  disable_savebutton();
}

function message(title, msg) {
  var html = '<h1>' + title + '</h1>';
  html += '<input type="button" onclick="clearmsg();" value="Close"/>';
  html += '<pre>' + msg + '</pre>';
  html += '<input type="button" onclick="clearmsg();" value="Close"/>';
  $('#message')[0].innerHTML = html;
  $('#message').css('display', 'block');
}

function update_sample_browser() {
  $.ajax({
    url: '/samples/find?' + $('form[name=samples]').serialize(),
    type: 'GET',
    dataType: 'json',
    success: function(data, textStatus) {
      $('select[name=samples]').html('');
      var list = $('select[name=samples]')[0];
      for (i in data) {
        var node = document.createElement('option');
        node.setAttribute('value', data[i]['name']);
        var text = document.createTextNode(data[i]['name']);
        node.appendChild(text);
        list.appendChild(node);
      }
    }
  });
}

function init_sample_browser() {
  $.ajax({
    url: '/samples/instruments',
    type: 'GET',
    dataType: 'json',
    success: function(result, textStatus) {
      $('select[name=instruments]').html('');
      var list = $('select[name=instruments]')[0];
      for (i in result) {
        var node = document.createElement('option');
        node.setAttribute('value', result[i]);
        var text = document.createTextNode(result[i]);
        node.appendChild(text);
        list.appendChild(node);
      }
    }
  });
  $.ajax({
    url: '/samples/games',
    type: 'GET',
    dataType: 'json',
    success: function(result, textStatus) {
      $('select[name=games]').html('');
      var list = $('select[name=games]')[0];
      for (i in result) {
        var node = document.createElement('option');
        node.setAttribute('value', result[i]);
        var text = document.createTextNode(result[i]);
        node.appendChild(text);
        list.appendChild(node);
      }
    }
  });
  $('select[name=games]').change(function() {
    update_sample_browser();
  });
  $('select[name=instruments]').change(function() {
    update_sample_browser();
  });
  update_sample_browser();
}

function close_sample_browser() {
  $('#samples').css('display', 'none');
  $('select[name=games]').unbind('change');
  $('select[name=instruments]').unbind('change');
}

var insert_selected_sample = function() {}

function getCaret(el) { 
  // Courtesy of user "CMS" on StackOverflow and because I'm lazy
  if (el.selectionStart) { 
    return el.selectionStart; 
  } else if (document.selection) { 
    el.focus(); 

    var r = document.selection.createRange(); 
    if (r == null) { 
      return 0; 
    } 

    var re = el.createTextRange(), 
        rc = re.duplicate(); 
    re.moveToBookmark(r.getBookmark()); 
    rc.setEndPoint('EndToStart', re); 

    return rc.text.length; 
  }  
  return 0; 
}

function open_sample_browser() {
  init_sample_browser();
  disable_player();
  $('#samples').css('display', 'block');
  var pos = getCaret($('textarea')[0]);
  insert_selected_sample = function() {
    var text = $('textarea').val();
    var samples = $('select[name=samples]').val();
    if (!samples)
      return false;
    while (pos < text.length && text[pos] != "\n") pos++;
    var sample = '@DPCM# = { "' + "samples/" + samples[0] + '.dmc", 15}';
    if (pos < text.length) {
      var first = text.substring(0, pos);
      var last = text.substring(pos);
      text = first + "\n" + sample + last;
    } else {
      text = text + "\n" + sample;
    }
    $('textarea').val(text);
    close_sample_browser();
  };
}

function update_line_number() {
  var pos = getCaret($('textarea')[0]);
  var text = $('textarea').val().substring(0, pos);
  var count = 1;
  for (var i=0; i<pos; i++) {
    if (text[i] == "\n")
      count++;
  }
  $('#line_number').html(count)
}

function init() {
  shortcut.add("Ctrl+I",function(e) {
    open_sample_browser();
    if (e.preventDefault) e.preventDefault();
    else e.returnValue = false;
    return false;
  });
  $('textarea').keyup(update_line_number);
  $('textarea').mouseup(update_line_number);
  $('input:checkbox').mouseup(update_line_number);
}

# NSF generation functions

import config

staticdir = config.basedir + '/static'
tooldir = config.basedir + '/tools'
sampledir = config.basedir + '/samples'

extensions = {'diskfm': "#EX-DISKFM\n",
              'namco106.1': "#EX-NAMCO106 1\n",
              'namco106.2': "#EX-NAMCO106 2\n",
              'namco106.3': "#EX-NAMCO106 3\n",
              'namco106.4': "#EX-NAMCO106 4\n",
              'namco106.5': "#EX-NAMCO106 5\n",
              'namco106.6': "#EX-NAMCO106 6\n",
              'namco106.7': "#EX-NAMCO106 7\n",
              'namco106.8': "#EX-NAMCO106 8\n",
              'vrc6': "#EX-VRC6\n",
              'vrc7': "#EX-VRC7\n",
              'mmc5': "#EX-MMC5\n",
              'fme7': "#EX-FME7\n"}

def generate_mml(track):
  mmlstring = """#TITLE %s
#COMPOSER %s
""" % (track['title'], track['artist'])
  try:
    if track['extensions']:
      for extname in track['extensions']:
        mmlstring += extensions[extname]
  except KeyError:
    pass
  return mmlstring + """#AUTO-BANKSWITCH 10

""" + track['body']
  

def run(cmd, cwd):
  import subprocess
  p = subprocess.Popen(cmd,
                       shell = True,
                       cwd = cwd,
                       stdin = subprocess.PIPE,
                       stdout = subprocess.PIPE,
                       close_fds = True)
  (child_stdin, child_stdout) = (p.stdin, p.stdout)
  return child_stdout.read()

def build_to_filename(mml, nsffilename):
  import tempfile
  import os
  import os.path
  import shutil

  sample_dir = sampledir
  base_dir = tooldir
  ppmckc_bin = base_dir + '/ppmckc_e'
  nesasm_bin = base_dir + '/nesasm'
  ppmckc_inc = [base_dir + '/ppmck', 
                base_dir + '/ppmck.asm',
                base_dir + '/ppmck.sym',
                sample_dir]

  build_dir = tempfile.mkdtemp()

  mmlfilename = build_dir + '/track.mml'

  mmlfile = open(mmlfilename, 'w+b')
  mmlfile.write(mml)
  mmlfile.close()

  asmfilename = build_dir + '/ppmck.asm'
  nesfilename = build_dir + '/ppmck.nes'

  results = 'Error building track.'
  try:
    for inc in ppmckc_inc:
      os.symlink(inc, build_dir + '/' + os.path.basename(inc))
    results = run('%s -i %s' % (ppmckc_bin, mmlfilename), build_dir)
    if not os.path.exists(build_dir + '/track.h'):
      raise Exception('Failed to compile MML')
    results += run('%s -s -raw %s' % (nesasm_bin, asmfilename), build_dir)
    if not os.path.exists(nesfilename):
      raise Exception('Failed to assemble ppmckc output')
    nsffile = open(nsffilename, 'w+b')
    nesfile = open(nesfilename, 'r+b')
    nsffile.write(nesfile.read())
    nesfile.close()
    nsffile.close()
  except:
    return results
  finally:
    shutil.rmtree(build_dir)
  return 'success'

def build_nsf(track_id, track):
  import os
  nsffilename = staticdir + '/nsf/' + str(track_id) + '.nsf'
  try:
    os.remove(nsffilename)
  except:
    pass
  return build_to_filename(track['body'].encode('utf-8'),
                           nsffilename)

def is_path_secure(s):
  import os.path
  return s == os.path.basename(os.path.abspath(s).replace('\\', '').replace('#','').replace('?', '').replace('&', ''))
